package com.enfocat.mvc;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

/**
 * DBConn
 */
public class DBConn {

    private static final String URL = "jdbc:mysql://localhost/contactostest";
	private static final String USERNAME = "admin";  // root 
	private static final String PASSWORD = "admin";

	public static Connection getConn() throws SQLException {
		DriverManager.registerDriver(new com.mysql.jdbc.Driver ());
		return (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}  
    
}
