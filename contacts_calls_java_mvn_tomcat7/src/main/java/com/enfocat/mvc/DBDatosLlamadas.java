package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


import com.mysql.jdbc.Connection;
/**
 * DBDatosLlamadas
 */
public class DBDatosLlamadas {

    private static final String TABLE = "llamadas";
    private static final String KEY = "id";
    private static final String FKEY = "id_contacto";

    public static Llamada newLlamada(Llamada ll) {
  
        String sql = "INSERT INTO llamadas (fecha, notas,id_contacto) VALUES (?,?,?)";

        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) { // lo que esta dentro de los () del try ; se cerrara solo

            long ms = ll.getFecha().getTime(); // la meva data en formar java.util.date
            java.sql.Date fecha_sql = new java.sql.Date(ms);// convertir en milisegons ,nova variable en format 
            pstmt.setDate(1, fecha_sql);                               
            pstmt.setString(2, ll.getNotas());
            pstmt.setInt(3, ll.getId_contacto());
            
            pstmt.executeUpdate();

            // usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
            if (rs.next()) {
                ll.setId(rs.getInt(1));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return ll;
    }

    public static Llamada getLlamadaId(int id){
        Llamada ll = null;
        String sql = String.format("select %s,fecha,notas,id_contacto from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
                    
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                ll = new Llamada( 
                    rs.getInt("id"),
                    rs.getDate("fecha"),
                    rs.getString("notas"),
                    rs.getInt("id_contacto")
                );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return ll;
    }

    public static boolean updateLlamada(Llamada ll){
        boolean updated = false;
        String sql = String.format("UPDATE llamadas set fecha=?, notas=?, id_contacto=? where id=%d", ll.getId());
    
        try (Connection conn = DBConn.getConn(); // que me haga la conexion
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            
        long ms = ll.getFecha().getTime(); // la meva data en formar java.util.date
        java.sql.Date fecha_sql = new java.sql.Date(ms);// convertir en milisegons ,nova variable en format 
        pstmt.setDate(1, fecha_sql);                               
        pstmt.setString(2, ll.getNotas());
        pstmt.setInt(3, ll.getId_contacto());
        pstmt.executeUpdate(); 
        updated = true;
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }

    public static boolean deleteLlamadaId(int id){
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted=true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }

    //llamada
    public static List<Llamada> getLlamadas() {
        List<Llamada> listaLlamadas = new ArrayList<Llamada>();  
        String sql = "select id,notas,fecha, id_contacto from llamadas"; 
        try (Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement()) { 
                
                ResultSet rs = stmt.executeQuery(sql);  
                while (rs.next()) {
                    Llamada nuevallamada = new Llamada(
                                rs.getInt("id"),
                                rs.getDate("fecha"),
                                rs.getString("notas"),
                                rs.getInt("id_contacto")
                                ); 
                    listaLlamadas.add(nuevallamada);
                    }
        
        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaLlamadas; 
    }
    
    public static List<Llamada> getLlamadasId(int id_num_contacto) {
        List<Llamada> listaLlamadas = new ArrayList<Llamada>();  
      String sql = String.format("select id,fecha,notas,id_contacto from llamadas where %s=%d",FKEY, id_num_contacto);
        try (Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement()) { 
                
                ResultSet rs = stmt.executeQuery(sql);  
                while (rs.next()) {
                    Llamada nuevallamada = new Llamada(
                                rs.getInt("id"),
                                rs.getDate("fecha"),
                                rs.getString("notas"),
                                rs.getInt("id_contacto")
                                ); 
                    listaLlamadas.add(nuevallamada);
                    }
        
        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaLlamadas; 
    }
    
}