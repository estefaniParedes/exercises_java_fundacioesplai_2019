package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;

/**
 * DatosLlamadas
 */
public class DatosLlamadas {

    private static List<Llamada> llamadas = new ArrayList<Llamada>();
    private static int ultimoLlamada = 0;

    static {
        llamadas.add(new Llamada(1, "2009-04-01", "llamada uno", 1));
        llamadas.add(new Llamada(2, "2010-03-02", "llamado dos",2 ));
        ultimoLlamada = 2;
    }

    public static Llamada newLlamada(Llamada ll) {
        ultimoLlamada++;
        ll.setId(ultimoLlamada);
        llamadas.add(ll);
        return ll;
    }

    public static Llamada getLlamadaId(int id) {
        for (Llamada ll : llamadas) {
            if (ll.getId() == id) {
                return ll;
            }
        }
        return null;// si no lo ha encontrado
    }

    // edita contacto
    public static boolean updateLlamada(Llamada ll) {
        boolean updated = false;
        for (Llamada x : llamadas) {
            if (ll.getId() == x.getId()) {
                int idx = llamadas.indexOf(x);
                llamadas.set(idx, ll);// machaco , remeplazo
                updated = true;
            }
        }
        return updated;
    }

    public static boolean deleteLlamadaId(int id) {
        boolean deleted = false;

        for (Llamada x : llamadas) {
            if (x.getId() == id) {
                llamadas.remove(x);
                deleted = true;
                break;
            }
        }
        return deleted;
    }

    public static List<Llamada> getLlamadas() {
        return llamadas;
    }
    
    public static List<Llamada> getLlamadasId(int id) {
        for (Llamada ll : llamadas) {
            if (ll.getId() == id) {
                return llamadas;
            }
        }
        return null;// si no lo ha encontrado
    }
}