package com.enfocat.mvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Llamada
 */
public class Llamada {

    private int id;
    private Date fecha;
    private String notas;
    private int id_contacto;

    // string format-> string a un objeto fecha
   
    // constructor in id
   
    public Llamada(String fecha, String notas, int id_contacto) {

        try {
            this.fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);  // si pongo otro formato me sale null . CORREGUIR 
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.notas = notas;
        this.id_contacto=id_contacto;
    }

    public Llamada(String fecha, String notas, String id_contacto) {

        try {
            this.fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);  // si pongo otro formato me sale null . CORREGUIR 
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.notas = notas;
        this.id_contacto = Integer.parseInt(id_contacto);
    }
    
    // constructor con id
    public Llamada(int id, String fecha, String notas, int id_contacto) {
        this.id = id;
        try {
            this.fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.notas = notas;
        this.id_contacto=id_contacto;
    }
    // constructor con fecha
    public Llamada(int id, Date fecha, String notas, int id_contacto) {
        this.id = id;
        this.fecha = fecha;
        this.notas = notas;
        this.id_contacto = id_contacto;
    }

    public Llamada(int id, String fecha, String notas, String id_contacto) {
        this.id = id;
        try {
            this.fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.notas = notas;
        this.id_contacto = Integer.parseInt(id_contacto);
    }
       
    
	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    
    public int getId_contacto() {
        return id_contacto;
    }

    public void setId_contacto(int id_contacto) {
        this.id_contacto = id_contacto;
    }

    @Override
    public String toString() {
        return "Llamada [fecha=" + fecha + ", id=" + id + ", id_contacto=" + id_contacto + ", notas=" + notas + "]";
    }

    
}