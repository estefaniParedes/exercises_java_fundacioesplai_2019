package com.enfocat.mvc;
import java.util.List;
/**
 * LlamadaController
 */
public class LlamadaController {

    static final String MODO = "db";
    // getAll devuelve la lista completa
    public static List<Llamada> getAll(){
        if (MODO.equals("db")){
            return DBDatosLlamadas.getLlamadas();
         }else{
        return DatosLlamadas.getLlamadas();
        }
    }

    public static List<Llamada> getAllId(int id_num_contacto){
        if (MODO.equals("db")){
            return DBDatosLlamadas.getLlamadasId(id_num_contacto);
         }else{
        return DatosLlamadas.getLlamadasId(id_num_contacto); 
        }
    }


    //getId devuelve un registro, getContactoById-> devuelve un objeto 
    public static Llamada getId(int id){
        if (MODO.equals("db")){
            return DBDatosLlamadas.getLlamadaId(id);
        }else{
        return DatosLlamadas.getLlamadaId(id);
        }
    }
   
    //save guarda un Contacto
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Llamada ll) {

        if (MODO.equals("db")){
            if (ll.getId() > 0) {
                DBDatosLlamadas.updateLlamada(ll);
            } else {
                DBDatosLlamadas.newLlamada(ll);
            }
        }else{
            if (ll.getId()>0){//si tiene un id mayor que 0.> existe ->modifica
                DatosLlamadas.updateLlamada(ll);
            } else {// no exite -> nuevo Llamada
                DatosLlamadas.newLlamada(ll);
            }
        }
        
    }

    // size devuelve numero de Contactos
    public static int size() {
        if (MODO.equals("db")){
            return DBDatosLlamadas.getLlamadas().size();
        }else{
        return DatosLlamadas.getLlamadas().size();
        }
    }

    // removeId elimina Contacto por id
    public static void removeId(int id){
        if (MODO.equals("db")){
            DBDatosLlamadas.deleteLlamadaId(id);
        }else{
           DatosLlamadas.deleteLlamadaId(id);
        }
    }
    
}