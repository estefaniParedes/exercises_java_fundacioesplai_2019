package com.enfocat.mvc.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/*El uso más común de los servlets es generar páginas 
web de forma dinámica a partir de los parámetros de la petición que envíe el navegador web. */

public class HolaServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Hola Servlet</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("Hola Servlet!");
        out.println("</body>");
        out.println("</html>");
    }
}
