<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>

<%

    Llamada ll = null;//creamos un contacto vacio
    boolean datos_guardados=false;

    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    if (id==null){ //http://localhost:8080/contactos/contacto/edita.jsp?nombre=eee--> mal --> va a la pagina princiapal
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/contactos");
        //IMPORTANTE! después de sendRedirect poner un RETURN!!!
        return;
    }else{
        // recibimos id, puede ser que
        // 1) llegue por GET, desde list.jsp: mostraremos los datos para que puedan ser editados
        // 2) llegue por POST, junto con el resto de datos, para guardar los cambios

        //verificamos si la petición procede de un POST
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, reccpilamos el resto de datos...
                
                int id_numerico = Integer.parseInt(id);
                String fecha = request.getParameter("fecha");
                String notas = request.getParameter("notas");
                String id_contacto = request.getParameter("id_contacto");// porque en el  input me da string 

                //creamos nuevo objeto contacto, que reemplazará al actual del mismo id
                ll = new Llamada(id_numerico,fecha,notas,id_contacto);
                LlamadaController.save(ll);
                datos_guardados=true;
                //redirigimos navegador a la página list.jsp
                response.sendRedirect("/contactos/contacto/listLlamada.jsp"); 
                return;
        } else {
            // hemos recibido un GET, solo tenemos un id
            // pedimos los datos del contacto para mostrarlos en el formulario de edifión
            ll = LlamadaController.getId(Integer.parseInt(id));
            if (ll==null) {//http://localhosLlamada/edita.jsp?id=2222 
                response.sendRedirect("/contactos");
                return;
            } 
        }
    }
%>

<!DOCTYPE html>
<html lang="es-ES">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
        integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>

<body>

    <%@include file="/menu.jsp"%>

    <div class="container">

        <div class="row">
            <div class="col">
                <h1>Editar Llamada</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">

                <form action="#" method="POST">
                    <!--va a rriba al post-->
                    <div class="form-group">
                        <label for="fechaInput">fecha del contacto</label>
                        <input name="fecha" type="date" min="2000-01-01" max="2018-12-25" class="form-control"
                            id="fechaInput" value="<%= ll.getFecha() %>">

                    </div>
                    <div class="form-group">
                        <label for="notasInput">notas del contacto</label>
                        <input name="notas" type="text" class="form-control" id="notasInput"
                            value="<%= ll.getNotas() %>">
                    </div>
                    <div class="form-group">
                        <label for="id_contactoInput">id_contacto del contacto</label>
                        <input name="id_contacto" type="text" class="form-control" id="id_contactoInput"
                            value="<%= ll.getId_contacto() %>">
                    </div>


                    <!-- guardamos id en campo oculto! para saber cual era el id del usuario que estaba modificando -> se enviara con el post pero esta allíi
datos que estan allí pero que el usurio no tiene porque saberlo (token, informacion que no tiene que estar moletando) -->
                    <input type="hidden" name="id" value="<%= ll.getId() %>">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
                <br>
                <br>
                <br>
                <% if (datos_guardados==true) { %>

                <h1 class="rojo">Datos guardados!</h1>
                <%}%>

</div>
</div>

</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
 


</body>
</html>