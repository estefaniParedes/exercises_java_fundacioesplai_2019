<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>

<%
//no lo haremos asi
//lo haremos con un semblet(clase de java): mucho java y poco html
//jsp es una forma facil de crear un semblet 

    String id = request.getParameter("id");
    if (id==null) {// si me llaman sin pasarme un id
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/contactos/index.jsp");
    } else {
        int id_numerico = Integer.parseInt(id);
        ContactoController.removeId(id_numerico);
        response.sendRedirect("/contactos/contacto/list.jsp");
    }
%>
