
package com.enfocat.javaweb.buscaminas;

import java.util.Random;

import javax.swing.text.GapContent;

public class Buscaminas {

    // constantes que cambiamos el tamaño del campo de minas
    private static final int ANCHO = 10;
    private static final int ALTO = 6;
    private static final int MINAS = 5; // bomba
    private static boolean gameOver = false;

    public static boolean isgameOver() {
        return gameOver;
    }

    // matriz filas y columnas -> objetos del tipo mina en este array
    private static Celda[][] campo = generaCampo(MINAS);
    public int status = 0;

    private static Celda[][] generaCampo(int minas_a_crear) {

        Celda[][] celdas = new Celda[ALTO][ANCHO]; // matriz de dos dimensiones

        // llenamos el campo de elementos Celda desativados
        for (int y = 0; y < ALTO; y++) {// filas
            for (int x = 0; x < ANCHO; x++) {// columnas
                Celda m = new Celda(false, x, y); // una mina es una celda : false: no bomba, x,y posicion
                celdas[y][x] = m;
            }
        }

        Random rnd = new Random();
        // activar las minas, no es MINAS es minas , el parametro
        for (int m = 0; m < minas_a_crear; m++) {
            int rndX = rnd.nextInt(ANCHO);
            int rndY = rnd.nextInt(ALTO);
            celdas[rndY][rndX].setMina(true);// que la bomba es true
        }

        return celdas;
    }

    public static Celda[][] getCampo() {
        return Buscaminas.campo;
    }

    public static void reset() {
        Buscaminas.campo = generaCampo(MINAS);
        gameOver = false;
    }

    public static void mostrar() {
        for (int y = 0; y < ALTO; y++) {// fila
            for (int x = 0; x < ANCHO; x++) {// columna
                Buscaminas.campo[y][x].setVisible(true);
                gameOver = true;
            }
        }

    }

    public static void clicar(int x, int y) {
        Celda m = Buscaminas.campo[y][x];
        m.setVisible(true); // [fila][columna]
        if (Buscaminas.campo[y][x].isMina()) {
            Buscaminas.mostrar();
            gameOver = true;
        }
        if (m.getContador() == 0 && !m.isMina()) {
            alrededor(x, y);
        }

    }

    // verificamos alrededor de una posición x,y
    public static void alrededor(int x, int y) {
        Buscaminas.revisa(x - 1, y - 1);
        Buscaminas.revisa(x, y - 1);
        Buscaminas.revisa(x + 1, y - 1);
        Buscaminas.revisa(x - 1, y);
        Buscaminas.revisa(x + 1, y);
        Buscaminas.revisa(x - 1, y + 1);
        Buscaminas.revisa(x, y + 1);
        Buscaminas.revisa(x + 1, y + 1);
    }

    public static void revisa(int x, int y) {

        if (x >= ANCHO || x < 0 || y < 0 || y >= ALTO) {
            return; 
        }
        Celda pos = Buscaminas.campo[y][x];

        if (pos.isMina() || pos.isVisible()) {
            return; 
        }

        if (pos.getContador() > 0) {
            pos.setVisible(true);
        }

        if (pos.getContador() == 0) {
            Buscaminas.clicar(x, y);
        }

    }

    public static boolean coordenadaDentro(int x, int y) {
        boolean dentro = true;
        // if ((x > 0 && x < ANCHO) && (y > 0 && y < ALTO)) { //x ancho y alto

        if (x >= ANCHO || x < 0 || y < 0 || y >= ALTO) {
            dentro = false;
        }

        return dentro;
    }

    public static int minasCercanas(int x, int y) { // celdas que hay alrrededor:x:horizontal y:vertical
        int contador = 0;

        if (coordenadaDentro(x - 1, y - 1) && Buscaminas.campo[y - 1][x - 1].isMina()) {
            contador++;
        }
        if (coordenadaDentro(x - 1, y) && Buscaminas.campo[y][x - 1].isMina()) {
            contador++;
        }
        if (coordenadaDentro(x - 1, y + 1) && Buscaminas.campo[y + 1][x - 1].isMina()) {
            contador++;
        }
        if (coordenadaDentro(x, y + 1) && Buscaminas.campo[y + 1][x].isMina()) {
            contador++;
        }
        if (coordenadaDentro(x + 1, y + 1) && Buscaminas.campo[y + 1][x + 1].isMina()) {
            contador++;
        }
        if (coordenadaDentro(x + 1, y) && Buscaminas.campo[y][x + 1].isMina()) {
            contador++;
        }
        if (coordenadaDentro(x + 1, y - 1) && Buscaminas.campo[y - 1][x + 1].isMina()) {
            contador++;
        }

        if (coordenadaDentro(x, y - 1) && Buscaminas.campo[y - 1][x].isMina()) {
            contador++;
        }

        return contador;
    }

    public static String finJuego(String contenido) {
        String plantilla = "<h2>%s xxx</h2>";
        return String.format(plantilla, contenido);
    }

    public static String htmlCampo() {

        StringBuilder sb = new StringBuilder();// crear un string a base de muchos fragmentos
        String iniDivFila = "<div class='fila'>";
        String finDiv = "</div>";

        sb.append("<div class='campo'>");
        for (int y = 0; y < ALTO; y++) {
            sb.append(iniDivFila);
            for (int x = 0; x < ANCHO; x++) {
                Celda m = Buscaminas.campo[y][x]; // mina que esta en esa posicion del array
                sb.append(m.html());// pido a cada mina que me devuelve su html
            }
            sb.append(finDiv); // final de fila , cuanod termino la fila se acaba el for de dentro y se va a la
                               // otra fila
        }
        sb.append(finDiv);

        return sb.toString(); // concatenar los fragmentos y devolvernos un unico string
    }

}