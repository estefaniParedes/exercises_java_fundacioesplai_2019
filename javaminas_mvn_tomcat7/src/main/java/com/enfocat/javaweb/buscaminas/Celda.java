package com.enfocat.javaweb.buscaminas;

public class Celda {
   
    private boolean visible;
    private int x;
    private int y;
    private boolean mina = false; // bomba
    private int contador = 0;

    public Celda(boolean mina, int x, int y) {
        this.mina = mina;
        this.x = x;
        this.y = y;
        this.visible = false;
    }

    public Celda(boolean visible, int x, int y, boolean mina, int contador) {
        this.visible = visible;
        this.x = x;
        this.y = y;
        this.mina = mina;
        this.contador = contador;
    }

    public int cambiarContador(int contador) {
        if (!this.mina) {
            this.contador = Buscaminas.minasCercanas(this.x, this.y);
        }
        return this.contador;
    }

    public String imagen(){  
        String plantilla;
       //https://fontawesome.com/
            plantilla = "<i class='fa fa-bomb'></i> ";
        return String.format(plantilla) ;
    }

    public String html() {
        String css = (this.mina) ? "celda mina " : "celda"; // mina es la celda oculta

        if (!this.visible)
            css += " oculta"; // pasa a la clase celda oculta
        if (this.mina) {
            return String.format("<div class='%s' data-x='%d' data-y='%d'>%s</div>", css, this.x, this.y,imagen() );
        } else {
            return String.format("<div class='%s' data-x='%d' data-y='%d'>%d</div>", css, this.x, this.y,
                    cambiarContador(this.contador));
        }
    }
    



    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isMina() {
        return mina;
    }

    public void setMina(boolean mina) {
        this.mina = mina;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

}
