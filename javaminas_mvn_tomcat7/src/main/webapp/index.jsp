<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.enfocat.javaweb.buscaminas.Buscaminas" %>

<%

    if (request.getParameter("reset")!=null){ // si recibes un parametro reset 
        Buscaminas.reset(); 
    }

   if (request.getParameter("mostrar")!=null){
        Buscaminas.mostrar();
    
    }
    
    if (request.getParameter("x")!=null){
        //request.getParameter : el objeto que me llega desde el servidor
        int x = Integer.parseInt(request.getParameter("x")); //clicar en un celda
        int y = Integer.parseInt(request.getParameter("y"));
        Buscaminas.clicar(x,y); 
        //Buscaminas.coordenadaDentro(x, y);
        //int celdasCercanas=Buscaminas.minasCercanas(x, y);
    }

    boolean estado=Buscaminas.isgameOver();
    
    
%>


<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilos.css">
</head>

<body>

    <h1>Buscaminas JSP</h1>

    <% if ( estado==true) { %>

    <p> <%= Buscaminas.finJuego("Game over")  %></p>


    <% } %>


    <%= Buscaminas.htmlCampo() %>
   


    <br>
    <br>
    <button id="reset">Reset</button>
    <button id="mostrar">Mostrar</button>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>

    <script>
        //son eventos que se lanzaran cuando tooque 
        //recargar la pagina pasandole ese parametro ;
        //pasamos datos a a traves de la url . otra forma seria a partir de un post 
        //http://localhost:8080/buscaminas/?x=7&y=0

        $(document).on("click", "div.celda.oculta", function () { // solo para las clase celda oculta
            let url = window.location.href.split("?")[0];//split -separa en un array de palabras sueltas , se parte es dos trozos lo que hay antes y despues del ?,y me quedo solo con lo despues del interrogante 
            let x = $(this).data("x");// me devuelven el valor x de esa celda : posicion de la celda
            // $(this) responde a ese elemento 
            let y = $(this).data("y");
            window.location.href = url + "?x=" + x + "&y=" + y;
        })

        $(document).on("click", "button#reset", function () {
            let url = window.location.href.split("?")[0];
            window.location.href = url + "?reset=1";
        })

        $(document).on("click", "button#mostrar", function () {
            let url = window.location.href.split("?")[0];
            window.location.href = url + "?mostrar=1";
        })
    </script>

</body>
<!-- // pulsamos sobre una mina se ha de acabar el juego : muestre todo el tablero , coun un get , 
//comprova alredor si hay bombas
//te dija las bombas que hay alrrededor : cuantas bombas hay alrededor , mirar alrededor-->

</html>