
package enfocat.javaweb;

public class HtmlFactory {

    public String titulo(String contenido) {
        String plantilla = "<h1>%s xxx</h1>";
        return String.format(plantilla, contenido);
    }

    public String lista(String[] contenido) {
        String x = "<ul class='listado'>";
        for (String s : contenido) {
            x += "<li>" + s + "</li>";
        }
        x = x + "</ul>";

        return x;
    }
    public String imagen(int num_jugada){  
        String plantilla;
        if (num_jugada==1) {
             plantilla = "<img src='imagenes/piedra.jpg' >";
        }else if(num_jugada==2){
            plantilla = "<img src='imagenes/papel.jpg' >";
        }else{
            plantilla = "<img src='imagenes/tijera.jpg' >";
        }
       
        return String.format(plantilla, num_jugada) ;
    }
   

}