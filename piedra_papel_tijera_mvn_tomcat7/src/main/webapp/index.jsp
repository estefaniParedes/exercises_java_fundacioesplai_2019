<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="enfocat.javaweb.HtmlFactory" %>
<%@page import="enfocat.javaweb.Datos" %>
<%@page import="enfocat.javaweb.Pipati" %>

<!--com.enfocat.javaweb, depende de lo que tengas -->

<%
    HtmlFactory factory = new HtmlFactory();   
%>

<!DOCTYPE html>
<html lang="es-ES">

<head>
    <meta charset="utf-8">
    <title>Java demo</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>

    <%= factory.titulo("Piedra Papel Tijera JSP") %>

    <img src="imagenes/piedra papel o tijera.jpg" alt="imagen general" class="imagen_principal">

    <!-- ? informacion que se pasa por el metodo get-->
    <ul>
<!--piedra--><li><a href="jugada.jsp?jugada=1"> <img src="imagenes/piedra.jpg" class="img1"> </a></li>
<!--papel--><li><a href="jugada.jsp?jugada=2"> <img src="imagenes/papel.jpg" class="img2"> </a></li>
<!--tijera--><li><a href="jugada.jsp?jugada=3"><img src="imagenes/tijera.jpg" class="img3"> </a></li>
    </ul>
</body>

</html>