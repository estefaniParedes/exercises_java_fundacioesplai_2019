<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="enfocat.javaweb.Pipati" %>
<%@page import="enfocat.javaweb.HtmlFactory" %>
<%
int jugada_usuario = Integer.parseInt(request.getParameter("jugada")); //1, request : objeto por defecto, lo convierte en un entero
String[] respuesta = Pipati.partida(jugada_usuario); //2 crear un array lo que ha jugado cada uno
String respuesta0 = respuesta[0];
String jugada_ordenador = respuesta[1];
HtmlFactory factory = new HtmlFactory();
%>
<!DOCTYPE html>
<html lang="es-ES">

<head>
    <meta charset="utf-8">
    <title>Java demo</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
    <h2> <%= respuesta0 %> </h2>
    <div>
        <%= factory.imagen(jugada_usuario) %>
        <p class="texto">VS</p>
        <%= factory.imagen(Integer.parseInt(jugada_ordenador)) %>
    </div>

    <a href="index.jsp">Volver a jugar</a>
</body>

</html>